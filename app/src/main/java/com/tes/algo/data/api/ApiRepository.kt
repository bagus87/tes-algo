package com.tes.algo.data.api

import com.tes.algo.model.request.RegisterRequest

class ApiRepository (private val apiInterface: ApiInterface) {

    suspend fun getImage() = apiInterface.getImage()
}