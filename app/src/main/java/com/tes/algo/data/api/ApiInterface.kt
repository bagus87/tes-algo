package com.tes.algo.data.api

import com.tes.algo.model.response.ImageResponse
import retrofit2.http.GET

interface ApiInterface {
    @GET("get_memes")
    suspend fun getImage(): ImageResponse

}