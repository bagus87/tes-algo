package com.tes.algo.data.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.tes.algo.data.api.ApiRepository
import com.tes.algo.model.Resource
import kotlinx.coroutines.Dispatchers

class BaseViewModel(private val apiRepository: ApiRepository) : ViewModel() {

    fun getImage() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = apiRepository.getImage()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}
