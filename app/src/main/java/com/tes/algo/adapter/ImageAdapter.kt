package com.tes.algo.adapter

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tes.algo.databinding.ItemImageBinding
import com.tes.algo.helper.viewBinding
import com.tes.algo.model.response.ImageResponse

class ImageAdapter(
    private val context: Context,
    var data: List<ImageResponse.Data.Meme>,
    val cellClickListener: CellClickListener
) :
    RecyclerView.Adapter<ImageAdapter.MyViewHolder>() {

    var posSelect = 999
    inner class MyViewHolder(binding: ItemImageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var imgImage = binding.imgImage
        var parentLy = binding.parentLayout

        fun bind(data: ImageResponse.Data.Meme, pos: Int) {
            Glide.with(context)
                .load(data.url)
                .into(imgImage)

            parentLy.setOnClickListener {
                cellClickListener.selectImage(data)
            }

        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ImageAdapter.MyViewHolder {
        return MyViewHolder(parent.viewBinding(ItemImageBinding::inflate))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = data!![position]
        holder.bind(data, position)
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    interface CellClickListener {
        fun selectImage(data: ImageResponse.Data.Meme)
    }
}

