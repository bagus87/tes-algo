package com.tes.algo.model

import android.net.Uri
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class DataImage (
    val fileName: String,
    val imageUri: Uri
): Parcelable