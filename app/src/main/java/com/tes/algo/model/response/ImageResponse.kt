package com.tes.algo.model.response

data class ImageResponse(
    val success: Boolean,
    val `data`: Data
) {
    data class Data(
        val memes: List<Meme>
    ) {
        data class Meme(
            val id: String,
            val name: String,
            val url: String,
            val width: Int,
            val height: Int,
            val box_count: Int,
            val captions: Int
        )
    }
}