package com.tes.algo.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
