package com.tes.algo.view.activity

import android.os.Bundle
import com.tes.algo.databinding.ActivityHomeBinding
import com.tes.algo.databinding.ActivityRegisterBinding
import com.tes.algo.helper.viewBinding
import com.tes.algo.view.base.BaseActivity

class RegisterActivity : BaseActivity() {
    private val binding by viewBinding(ActivityRegisterBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setListener()
    }


    private fun setListener(){

    }
}