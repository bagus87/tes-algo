package com.tes.algo.view.fragment

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.tes.algo.R
import com.tes.algo.adapter.ImageAdapter
import com.tes.algo.data.viewModel.BaseViewModel
import com.tes.algo.data.viewModel.ViewModelFactory
import com.tes.algo.databinding.FragmentHomeBinding
import com.tes.algo.helper.viewBinding
import com.tes.algo.model.Status
import com.tes.algo.model.response.ImageResponse
import com.tes.algo.view.base.BaseFragment

class HomeFragment : BaseFragment(R.layout.fragment_home), ImageAdapter.CellClickListener {

    private val binding by viewBinding(FragmentHomeBinding::bind)

    private lateinit var baseViewModel: BaseViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModel()
        getImage()
        setView()
        setListener()
    }

    private fun initViewModel(){
        baseViewModel = ViewModelProviders.of(
            this, ViewModelFactory(apiInterface)
        ).get(BaseViewModel::class.java)
    }

    private fun getImage(){
        baseViewModel.getImage().observe(viewLifecycleOwner){
            it?.let { resource ->
                when(resource.status){
                    Status.SUCCESS -> {
                        val data = resource.data?.data?.memes
                        binding.rvImage.layoutManager = GridLayoutManager(requireContext(), 3)
                        binding.rvImage.adapter =
                            data?.let { it1 -> ImageAdapter(requireContext(), it1, this) }
                        pLoading.dismissDialog()
                    }
                    Status.ERROR -> {
                        pLoading.dismissDialog()
                    }
                    Status.LOADING -> {
                        pLoading.showLoading()
                    }
                }
            }
        }
    }

    private fun setListener() {
        binding.imgRefresh.setOnClickListener {
            getImage()
        }
    }

    private fun setView(){
    }

    override fun selectImage(data: ImageResponse.Data.Meme) {
        findNavController().navigate(R.id.detailImageFragment, bundleOf("imageUrl" to data.url))
    }

}