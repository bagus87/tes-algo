package com.tes.algo.view.fragment

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import com.tes.algo.R
import com.tes.algo.databinding.FragmentShareImageBinding
import com.tes.algo.helper.viewBinding
import com.tes.algo.model.DataImage
import com.tes.algo.view.base.BaseFragment


class ShareImageFragment : BaseFragment(R.layout.fragment_share_image){

    private val binding by viewBinding(FragmentShareImageBinding::bind)

    private var dataImage: DataImage?= null

    private val locationFolder = "ALGO"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataImage = arguments?.getParcelable("dataImage")

        setView()
        setListener()
    }

    private fun setListener(){
        binding.imgBack.setOnClickListener {
            activity?.onBackPressed()
        }

        binding.btnShareFb.setOnClickListener {
            shareTo("com.facebook.katana")
        }

        binding.btnShareTwitter.setOnClickListener {
            shareTo("com.twitter.android")
        }
    }

    private fun setView(){
        if (dataImage != null){
            binding.imgImage.setImageURI(dataImage?.imageUri)
        }
    }

    private fun shareTo(toPackage: String){
        val bitmap = (binding.imgImage.drawable as BitmapDrawable).bitmap

        val path = MediaStore.Images.Media.insertImage(
            requireContext().contentResolver,
            bitmap,
            "Image Description",
            null
        )
        val uri = Uri.parse(path)

        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "image/jpeg"
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        intent.setPackage(toPackage)
        startActivity(Intent.createChooser(intent, "Share this via"))
    }

    private fun getBitmapFromView(view: View, height: Int, width: Int): Bitmap {
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val bgDrawable = view.background
        if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
        view.draw(canvas)
        return bitmap
    }
}