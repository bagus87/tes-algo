package com.tes.algo.view.fragment

import android.app.Activity
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tes.algo.R
import com.tes.algo.databinding.FragmentDetailImageBinding
import com.tes.algo.helper.viewBinding
import com.tes.algo.model.DataImage
import com.tes.algo.view.base.BaseFragment
import java.io.File
import java.io.FileNotFoundException
import java.io.OutputStream

import java.text.SimpleDateFormat
import java.util.Date

class DetailImageFragment : BaseFragment(R.layout.fragment_detail_image){

    private val binding by viewBinding(FragmentDetailImageBinding::bind)

    private var imageUrl = ""

    private var fileImage : File?= null
    private var imgUri : Uri?= null

    private val locationFolder = "ALGO"
    private var fileName = ""

    private var fileNameSs = ""

    private var dataImage: DataImage?= null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageUrl = arguments?.getString("imageUrl")?: ""

        setView()
        setListener()
    }

    private fun setListener(){
        binding.imgBack.setOnClickListener {
            activity?.onBackPressed()
        }

        binding.lyAddLogo.setOnClickListener {
            ImagePicker.with(this)
                .galleryOnly()
                .maxResultSize(640, 640)
                .start { resultCode, data ->
                    when (resultCode) {
                        Activity.RESULT_OK -> {
                            fileImage = ImagePicker.getFile(data)!!
                            Log.d("dataImage", data?.data.toString())

                            imgUri = data?.data

                            val filePath: String = fileImage!!.path

                            fileName = (getFileName(requireContext(), imgUri!!)?: "").replaceFirst("/", "")

                            binding.imgImage.setImageURI(data?.data)
                        }
                        ImagePicker.RESULT_ERROR -> {
                            Toast.makeText(requireContext(), ImagePicker.getError(data).toString(), Toast.LENGTH_SHORT).show()
                        }
                        else -> {
                            Toast.makeText(requireContext(), "Task Cancelled", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

        }

        binding.lyAddText.setOnClickListener {
            showbottomSheetAddText()
        }

        binding.btnSave.setOnClickListener {
            saveScreenshoot()
        }
    }

    private fun getFileName(context: Context, uri: Uri): String? {
        if (uri.scheme == "content") {
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor.use {
                if (cursor != null) {
                    if(cursor.moveToFirst()) {
                        return cursor.getString(cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME))
                    }
                }
            }
        }
        return uri.path?.lastIndexOf('/')?.let { uri.path?.substring(it) }
    }


    private fun saveImageInternal(bitmaps: Bitmap, filename: String) {
        val outputStream: OutputStream
        val contentResolver: ContentResolver
        contentResolver = requireContext().contentResolver
        val contentValues = ContentValues()
        contentValues.put(MediaStore.Downloads.DISPLAY_NAME, filename)
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
        contentValues.put(
            MediaStore.MediaColumns.RELATIVE_PATH,
            Environment.DIRECTORY_PICTURES + "/" + locationFolder
        )

        val fileUri =
            contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
        try {
            outputStream = requireContext().contentResolver.openOutputStream(fileUri!!)!!
            val bitmap: Bitmap = bitmaps
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)

            dataImage = DataImage(
                fileNameSs, fileUri
            )
            toast("Gambar berhasil disimpan")

            findNavController().navigate(R.id.shareImageFragment, bundleOf("dataImage" to dataImage))

        } catch (e: FileNotFoundException) {
            throw RuntimeException(e)
        }
    }

    //create bitmap from the ScrollView
    private fun getBitmapFromView(view: View, height: Int, width: Int): Bitmap {
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val bgDrawable = view.background
        if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
        view.draw(canvas)
        return bitmap
    }


    private fun saveScreenshoot(){
        var bitmap = getBitmapFromView(
            binding.clImage,
            binding.clImage.height,
            binding.clImage.width
        )

        bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true)

        val sdf = SimpleDateFormat("ss_mm_hh_dd_MM_yyyy")
        val currentDate = sdf.format(Date())

        //save the bitmap image
        val imagesDir =
//            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
            Environment.DIRECTORY_PICTURES + "/" + locationFolder

        val root = File(imagesDir, locationFolder)
        if (!root.exists()) {
            root.mkdir()
        }

        fileNameSs = "SS_$currentDate.jpg"
        val file = File(root, fileNameSs)
        try {
            saveImageInternal(bitmap, fileNameSs)

        } catch (e: Exception) {
            e.stackTrace
            toast( "Failed save image , " + e.stackTrace.toString())
        }

    }

    private fun setView(){
        Glide.with(requireContext())
            .load(imageUrl)
            .into(binding.imgImage)
    }

    private fun showbottomSheetAddText(){
        val dialog = BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)

        val view = layoutInflater.inflate(R.layout.bottom_sheet_add_text, null)
        val tvName = view.findViewById<EditText>(R.id.edtText)
        val btnSave = view.findViewById<Button>(R.id.btnSave)

        btnSave.setOnClickListener {
            if (tvName.text.toString().isEmpty()){
                toast("Text harus diisi!")
                return@setOnClickListener
            }

            binding.tvText.text = tvName.text.toString()

            dialog.cancel()
        }

        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()
    }
}