package com.tes.algo.view.activity

import android.os.Bundle
import com.tes.algo.databinding.ActivityLoginBinding
import com.tes.algo.helper.viewBinding
import com.tes.algo.view.base.BaseActivity

class LoginActivity : BaseActivity() {
    private val binding by viewBinding(ActivityLoginBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setListener()
    }


    private fun setListener(){

    }
}